/*
 * sqrt.c   sqrt
 *
 * Copyright (C) 2014, 2018   Caipenghui   蔡鹏辉   All Rights Reserved.
 *
 * Email: <Caipenghui_c@163.com>
 *
 *   ∧_∧::
 *  (´･ω･`)::
 *  /⌒　　⌒)::
 * /へ_＿  / /::
 * (＿＼＼  ﾐ)/::
 * ｜ `-イ::
 * /ｙ　 )::
 * /／  ／::
 * ／　／::
 * (　く:::
 * |＼ ヽ:::
 */

#include <stdio.h>
#include <math.h>

int main(void)
{
         double x;
         scanf("%lf",&x);
         if (x >= 0)
                 printf("%10.6lf",sqrt(x));   /* 调用math库 sqrt函数原型为 double sqrt (double x);
         return 0;
}
